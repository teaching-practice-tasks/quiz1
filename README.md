## Questions for Quiz 1
By topic:
### Input, Output, Variables, Consts

1. What is the output of the following code?
```
x = 10
y = 20
print(x + y)
```
- [x] 30
- [ ] 10
- [ ] 20
- [ ] 1020

2. What is the output of the following code?

```
GRAVITY = 9.81

GRAVITY += 1
print(GRAVITY)
```

- [ ] 9.91
- [ ] Error
- [ ] 9.81
- [x] 10.81

3. Which line of the following code will produce an error?

```
x = [1, 2, 3, 4, 5]
y = x
y[2] = "zero"
result = x[2] + 10
```

- [ ] x = [1, 2, 3, 4, 5]
- [x] result = x[2] + 10
- [ ] y = x
- [ ] y[2] = "zero"

4. Choose input for this code so that `result` will be equal to 6:

```
x = input()
result = int(x) / 2
print(result)
```

- [ ] AB
- [X] 12 
- [ ] twelve
- [ ] 1 2

5. Which line of the following code will produce an error?

```
x = "hello"
y = x.upper()
z = y.reverse()
```

- [ ] x = "hello"
- [X] z = y.reverse()
- [ ] y = x.upper()

### Conditionals, Functions

1. What will be the output of the following code?

```
def even_or_odd(x):
    if x % 2 == 0:
        return "even"
    else:
        return "odd"

print(even_or_odd(3 + 4))
```

- [X] odd
- [ ] even
- [ ] Error

2. What is the output of the following code?

```
def print_name(name):
    if len(name) > 5:
        print("My name is " + name)
    else:
        return "Name too short"

print(print_name("Bob"))
print(print_name("Robert"))
```

- [ ] Name too short
Name too short
- [x] Name too short
My name is Robert
- [ ] My name is Bob
My name is Robert
- [ ] My name is Bob
Name too short

3. What will be the output of the following code?

```
def concatenate_strings(x, y):
    return x + y

x = ["Hello", "world"]
result = concatenate_strings(x[0], x[1])
print(result)
```

- [ ] Hello world
- [ ] Hw
- [ ] He
- [X] Helloworld

4. What is the output of the following code?

```
def return_largest(a, b, c):
    if a > b and a > c:
        return a
    elif b > a and b > c:
        return b
    else:
        return c

print(return_largest(1, 2, 3))
print(return_largest(3, 2, 1))
print(return_largest(2, 3, 1))
```

- [X] 3 3 3
- [ ] 2 2 3
- [ ] 3 3 2
- [ ] 3 2 3

5. What is the output of the following code?


```
def check_divisibility(x,y):
    if x % y == 0:
        return x/y
    else:
        return False

x = 20
y = check_divisibility(x,4)
print(y)
```

- [ ] False
- [ ] True
- [X] 5 

### Loops, Recursion

1. What is the output of the following code?

```
x = 0
for i in range(5):
    x += i
print(x)
```

- [ ] 12345
- [ ] 5
- [X] 10
- [ ] 0

2. What will be the output of the following code?

```
def factorial(n):
    if n == 1:
        return 1
    else:
        return n * factorial(n-1)
print(factorial(3))
```

- [X] 6
- [ ] 2
- [ ] 3
- [ ] 24

3. What is the output of the following code?

```
x = [1, 2, 3, 4, 5]
result = [i**2 for i in x]
print(result)
```

- [ ] 2, 4, 6, 8, 10
- [ ] 1, 2, 3, 4, 5
- [ ] 1, 4, 9, 15, 24
- [X] 1, 4, 9, 16, 25

4. What will be the output of the following code?

```
x = 1
while x <= 5:
    print(x)
    x += 1
```

- [X] 1 2 3 4 5
- [ ] 1 1 1 1 1
- [ ] Infinite loop
- [ ] 5 5 5 5 5
- [ ] 1 2 3 4 5 6

5. What will be the output of the following code?

```
x = [1, 2, 3, 4, 5]
result = 0
for i in x:
    if i == 3:
        continue
    result += i
print(result)
```

- [ ] 11
- [ ] 13
- [X] 12
- [ ] 15

### Lists, Tuples, String 

1. What is the output of the following code?

```
x = [1, 2, 3, 4, 5]
x[1:3] = [0]
print(x)
```

- [ ] [1, 0, 3, 4, 5]
- [X] [1, 0, 4, 5]
- [ ] [1, 2, 0, 4, 5]
- [ ] [1, 2, 0, 5]
- [ ] [0, 3, 4, 5]

2. What will be the output of the following code?

```
x = (1, 2, 3)
y = list(x)
y[1] = 0
x = tuple(y)
print(x)
```

- [X] (1, 0, 3)
- [ ] (0, 2, 3)
- [ ] (1, 2, 0)

3. What will be the output of the following code?

```
x = "hello"
y = x[1:4]
y = y[::-1]
print(y)
```

- [ ] ll
- [X] lle
- [ ] lleh
- [ ] le

4. What will be the output of the following code?

```
x = ["apple", "banana", "cherry"]
y = " ".join(x)
print(y)
```

- [ ] apple bananacherry
- [ ] applebananacherry
- [X] apple banana cherry

5. What will be the output of the following code?

```
x = (1, 2, 3, 4, 5)
result = x[::-2]
print(result)
```

- [ ] (5, 4, 3, 2, 1)
- [X] (5, 3, 1)
- [ ] (1, 3, 5)
- [ ] (1, 2, 3, 4, 5)

### Sets, Dictionaries

1. What will be the output of the following code?

```
x = {1: 'a', 2: 'b', 3: 'c'}
y = x.pop(2)
print(y)
```

- [X] b
- [ ] a
- [ ] c
- [ ] 2

2. What will be the output of the following code?

```
x = {"a":1,"b":2,"c":3}
y = x.get("d",4)
print(y)
```

- [ ] 0
- [X] 4
- [ ] d
- [ ] Error

3. What is the output of the following code?

```
x = {1, 2, 3, 4, 5}
y = x.union({3, 4, 5, 6, 7})
print(y)
```

- [X] {1,2,3,4,5,6,7}
- [ ] {3,4,5}
- [ ] {1,2,6,7}
- [ ] {1,2,3,4,5}
- [ ] {3,4,5,6,7}

4. What will be the output of the following code?

```
x = {1: "a", 2: "b", 3: "c"}
y = x.fromkeys([1,2], "b")
print(y)
```

- [ ] {1: 'a', 2: 'b', 3: 'c'}
- [ ] {1: 'a', 2: 'b'}
- [X] {1: 'b', 2: 'b'}
- [ ] {1: 'b'}

5. What will be the output of the following code?

```
x = {1: 'a', 2: 'b', 3: 'c'}
y = x.copy()
x = y.clear()
print(x)
```

- [X] None
- [ ] {}
- [ ] Error
- [ ] {1: 'a', 2: 'b', 3: 'c'}